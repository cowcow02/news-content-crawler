import os

from pymongo import MongoClient
from pymongo.collection import Collection


def get_mongo_connection():
    client = MongoClient(os.getenv('MONGO_URI'))
    database = client[os.getenv('MONGO_DATABASE_NAME')]
    return database


def get_domain(domain_id):
    data = [
        {
            "id": 1,
            "domain": "arc-photo-appledaily.s3.amazonaws.com"
        },
        {
            "id": 2,
            "domain": "cloudfront-ap-northeast-1.images.arcpublishing.com"
        },
        {
            "id": 3,
            "domain": "d87urpdhi5rdo.cloudfront.net"
        },
        {
            "id": 4,
            "domain": "d2i91erehhsxi2.cloudfront.net"
        },
        {
            "id": 5,
            "domain": "video.appledaily.com.hk"
        },
        {
            "id": 6,
            "domain": "arc-static.appledaily.com"
        },
        {
            "id": 7,
            "domain": "s3.amazonaws.com"
        },
        {
            "id": 8,
            "domain": "tw-arc-static.s3.amazonaws.com"
        },
        {
            "id": 9,
            "domain": "video.next.hk"
        }
    ]
    return next((x for x in data if x.get('id') == domain_id), {}).get('domain')


def serialize(record):
    record.pop('_id')
    record['url'] = f"https://{get_domain(record['domainId'])}{record['path']}"
    return record


def get_not_uploaded_urls(count=100):
    database = get_mongo_connection()
    return [serialize(v) for v in
            database.Media.find({
                'is_started_uploading': {'$ne': True},
                'path': {'$regex': "(jpg)|(jpeg)|(png)|(bmp)|(webp)$"},
            }).sort('id', -1).limit(count)]


def mark_as_started(ids):
    database: Collection = get_mongo_connection().Media
    return database.update_many({'id': {'$in': ids}}, {'$set': {'is_started_uploading': True}})


def mark_as_done(ids):
    database: Collection = get_mongo_connection().Media
    return database.update_many({'id': {'$in': ids}}, {'$set': {'is_uploaded': True}})
