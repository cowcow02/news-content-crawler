from http import HTTPStatus


class TestGet:
    def test_that_it_returns_ok_status_code(self, client):
        response = client.get('/hello')
        assert response.status_code == HTTPStatus.OK

    def test_that_it_returns_correct_data(self, client):
        response = client.get('/hello')
        assert response.json == {
            'success': True
        }
