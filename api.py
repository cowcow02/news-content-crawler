from flask import Flask, jsonify, request

import events
from exception_classes import BadRequest
from mongo import get_not_uploaded_urls

app = Flask(__name__)
app.url_map.strict_slashes = False


@app.route("/hello", methods=['GET'])
def hello():
    return jsonify({
        'success': True
    })


@app.route("/test-push-job", methods=['GET'])
def test_push_job():
    count = int(request.args.get('count', 100))
    media_records, message_id = events.fetch_media_to_sqs(count)
    return jsonify({
        'success': True,
        'sqs_message_id': message_id,
        'results': media_records,
    })


@app.route("/test-receive-job", methods=['GET'])
def test_receive_job():
    count = int(request.args.get('count', 1))
    media_records = get_not_uploaded_urls(count)
    results = events.process_received_media_records(media_records)
    return jsonify({
        'success': True,
        'results': results,
    })


@app.errorhandler(BadRequest)
def handle_bad_request(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response
