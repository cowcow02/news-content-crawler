import io
import json
import logging
import os
import time

import boto3
import requests

from mongo import get_not_uploaded_urls, mark_as_started, mark_as_done

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def fetch_media_to_sqs(count=100):
    records = get_not_uploaded_urls(count)
    return push_to_sqs(records)


def push_to_sqs(records):
    media_to_push = [{
        'id': v.get('id'),
        'url': v.get('url'),
        'path': v.get('path'),
    } for v in records]
    sqs = boto3.client('sqs')
    response = sqs.send_message(
        QueueUrl=os.getenv('SQS_QUEUE_URL'),
        MessageBody=json.dumps(media_to_push),
    )
    if response:
        mark_as_started([v.get('id') for v in records])
    return media_to_push, response['MessageId']


def trigger_download_jobs(event, context):
    size = 10000
    batch_size = 100
    batch = []
    time_start = time.time()
    logger.info(f"job started.")
    records = get_not_uploaded_urls(size)
    logger.info(f"url retrieved, {len(records)} records to push.")
    for record in records:
        batch.append(record)
        if len(batch) == batch_size:
            push_to_sqs(batch)
            logger.info(f"batch pushed, last id: {record.get('id')}")
            batch = []
    logger.info(f"job complete, {size} records pushed to sqs. Elasped time: {time.time() - time_start}")


def download_media_to_s3(event, context):
    records = event['Records']
    for record in records:
        process_received_media_records(json.loads(record['body']))


def process_received_media_records(media_records: list):
    session = boto3.Session(
        aws_access_key_id=os.getenv('UPLOAD_AWS_ACCESS_KEY'),
        aws_secret_access_key=os.getenv('UPLOAD_AWS_SECRET_KEY'),
    )
    bucket_name = os.getenv('UPLOAD_S3_BUCKET_NAME')
    s3 = session.resource('s3')
    ids_uploaded = []
    for image in media_records:

        url = image.get('url')
        response = requests.get(url, stream=True)
        content_type = response.headers['content-type']
        data = response.raw.data
        bytesIO = io.BytesIO()
        bytesIO.write(data)
        bytesIO.seek(0)
        path = image.get('path').strip('/')
        object = s3.Object(bucket_name, path).upload_fileobj(bytesIO, ExtraArgs={
            'CacheControl': 'max-age=31536000,public,immutable',
            'ContentType': content_type
        })
        bytesIO.close()
        ids_uploaded.append(image.get('id'))
    mark_as_done(ids_uploaded)
    return ids_uploaded
